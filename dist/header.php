<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="jquery/jquery-ui.css">
    <link rel="stylesheet" href="jquery/jquery-ui.structure.css">
    <link rel="stylesheet" href="jquery/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="font/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./css/main.css">
    <title>Document</title>
</head>
<body>
   
   
    <div class="header">
<div class="container head-container">
            <div class="header-inner head-descktop">
                <div class="col-xl-3 col-md-3 col-lg-3 pl-0">
                <div class="logo">
                    <img src="image/logo.png" alt="">
                </div>
            </div>
            <div class="col-xl-9 col-md-9 col-lg-9">
                <div class="header-links">
                    <nav class="nav-links">
                            <a href="index.php" class="link active">ГЛАВНАЯ</a>
                            <a href="we.php">МЫ</a>
                            <a href="about.php">О КОМПАНИИ</a>
                            <a href="vacancy.php">ВАКАНСИИ</a>
                            <a href="contact.php">КОНТАКТЫ</a>
                    </nav>
                </div>
            </div>
         
        </div>
        
        <div class="burger-menu">
            <div class="burger-menu-image">
            <img src="image/logo.png" alt="">
            </div>
        <div class="burger-menu-content">
        <a href="#" class="burger-menu-btn">
			<span class="burger-menu-lines"></span>
		</a>
      
    </div>
    <div class="nav-panel-mobil">
		<div class="container">
			<nav class="navbar-expand-lg navbar-light">
				<ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">
					<li class="nav-item">
						<a class="nav-link" href="#">О компании</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="contact.html">Контакты</a>
					</li>
					 <li class="nav-item">
            <a class="nav-link" href="news.html">Новости</a>
         </li>
         <li class="nav-item">
            <a class="nav-link"  href="delivery.html">Оплата и доставка</a>
         </li>
        <li class="nav-item">
            <a class="nav-link"  href="#">Жалобы и предложения</a>
         </li>
    
    </ul>

			</nav>
		</div>
        </div>
        </div>
</div>
</div>