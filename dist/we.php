<?php include('header.php');?>
<div class="we">
<div class="container">
    <div class='crumb-page'>
    <nav aria-label="breadcrumb ">
  <ol class="breadcrumb crumbs">
    <li class="breadcrumb-item crumb-item"><a href="#">Главная</a></li>
    <li class="breadcrumb-item active" aria-current="page">Мы
    </li>
  </ol>
</nav>
</div>
<div class="we-slider">
    <div class="container">
        <div class="sliderWe">
            <div class="slider-content">
              <a href="">
            <div class="slider-logo">
              <img src="image/herkul-logo.png" alt="">  
            </div>
            </a>
            <div class="slider-txt">
                <p>Был запущен первый завод по производству труб в Казахстане - <b>herkul (ТОО "Fidelis Global")</b></p>
            </div>
            <div class="slider-icon">   
                <img src="image/slider-circle.png" alt="">
                <p>1997</p>
            </div>
            </div>
            <div class="slider-content">
              <a href="">
            <div class="slider-logo">
              <img src="image/alugal-logo.png" alt="">  
            </div>
            </a>
            <div class="slider-txt">
                <p>Был открыт один из первых заводов по выпуску алюминиевых профилей в Казахстане - <b>alugal (ТОО «Alugal»)</b></p>
            </div>
            <div class="slider-icon">   
                <img src="image/slider-circle.png" alt="">
                <p>2002</p>
            </div>
            </div>
            <div class="slider-content">
              <a href="">
            <div class="slider-logo">
              <img class="sieger" src="image/sieger-we.png" alt="">  
            </div>
            </a>
            <div class="slider-txt">
                <p>К компании присоединился <b>Фатих Узуноглу</b> в качестве Главного управляющего компанией</p>
            </div>
            <div class="slider-icon">   
                <img src="image/slider-circle.png" alt="">
                <p>2005</p>
            </div>
            </div>
            <div class="slider-content">
              <a href="">
            <div class="slider-logo">
              <img src="image/galwin-logo.png" alt="">  
            </div>
            </a>
            <div class="slider-txt">
                <p>Открылся первый завод по производству ПВХ профилей в Алматы - <b>galwin (ТОО «NOVUS POLYMER»).</b></p>
            </div>
            <div class="slider-icon">   
                <img src="image/slider-circle.png" alt="">
                <p>2006</p>
            </div>
            </div>
            <div class="slider-content">
              <a href="">
            <div class="slider-logo">
              <img src="image/nedex.png" alt="">  
            </div>
            </a>
            <div class="slider-txt">
                <p>Началось производство аксессуаров для стеклопакетов с группой компаний Nedex  -  <b>ТОО «NEDEX ALMATY»</b> </p>
            </div>
            <div class="slider-icon">   
                <img src="image/slider-circle.png" alt="">
                <p>2012</p>
            </div>
            </div>
            <div class="slider-content">
              <a href="">
            <div class="slider-logo">
              <img src="image/nedex.png" alt="">  
            </div>
            </a>
            <div class="slider-txt">
                <p>Началось производство аксессуаров для стеклопакетов с группой компаний Nedex  -  <b>ТОО «NEDEX ALMATY»</b> </p>
            </div>
            <div class="slider-icon">   
                <img src="image/slider-circle.png" alt="">
                <p>1997</p>
            </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php');?>