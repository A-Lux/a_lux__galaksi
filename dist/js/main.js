  $(document).ready(function(){
    $('.link-overflow').click(function(e){
      e.preventDefault();
      $('.text-overflow').toggleClass('text-overflow-active')
      console.log('overflow')
    });


    $('.slide-show').slick({
        slidesToShow: 4,
        infinite: false,
       slidesToScroll: 1,
       autoplay: true,
       responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
      });

      $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
      });
      $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        focusOnSelect: true
      });
      
       $('.burger-menu-btn').click(function(e){
		e.preventDefault();
		$(this).toggleClass('burger-menu-lines-active');
		$('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
		$('body').toggleClass('body-overflow');
  });
  $('.sliderWe').slick({
    infinite: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});