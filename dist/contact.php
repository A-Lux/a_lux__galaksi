<?php include('header.php');?>
<div class="contact">
<div class="container">
    <div class='crumb-page'>
    <nav aria-label="breadcrumb ">
  <ol class="breadcrumb crumbs">
    <li class="breadcrumb-item crumb-item"><a href="#">Главная</a></li>
    <li class="breadcrumb-item active" aria-current="page">Контакты
    </li>
  </ol>
</nav>
</div>
</div> 
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="contact-title">
                <h1>Контакты</h1>
                </div>
            </div>
</div>
                <div class="contact-inner">
                <div class="row ">
                <div class="col-xl-3 pr-0 col-md-3">
                <div class="contact-content">
                        <h2>8 727 399 48 11</h2>
                        <div class="email">
                        <a href=""> <b>E-mail:</b> marketing@siegeria.com </a>
                        </div>
                        <p> <b>Адрес:</b> пр. Рыскулова, 1/7, г. Алматы Республика Казахстан </p>
                        <p>Почтовый индекс 050019</p>
                </div>
                </div>
                <div class="col-xl-9 col-12 col-md-9">
                <div class="map">
                <iframe class="maps" width="100%" src="https://yandex.ru/map-widget/v1/?um=constructor%3A31e0d802b34a9febc797f2cc173f19079c16b90423749bc97c7494763bc855a9&amp;source=constructor"  frameborder="0"></iframe>
                </div>
                </div>
                </div>
            </div>   
</div>
</div>
<div class="contact-form">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-5">
                        <div class="contact-item">
                         <h2>Оставьте свое сообщение  и контакты, мы с вами  свяжемся</h2>
                         <p>+ дадим полезную информацию</p>
                         <div class="contact-icon">
                             <div class="contact-image">
                             <img src="image/talking.png" alt="">
                             </div>
                             <div class="contact-info">
                             <p>Перезвоним в течение дня, с 9:00 до 18:00 </p>
                </div>
                         </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-md-7">
                        <div class="contact-link">
                        <div class="input-forms">
                            <div class="row">
                            <div class="col-xl-4 col-12 pr-0">
                                <div class="forms">
                                    <input type="text" placeholder='Имя'>
                                </div>
                            </div>
                            <div class="col-xl-4 col-12 pr-0">
                                    <input type="text" placeholder='Телефон'>
                            </div>
                            <div class="col-xl-4 col-12 pr-0">
                                <div class="forms">
                                    <input type="text" placeholder='Эл.почта'>
                                </div>
                            </div>
                           
                        <div class="col-xl-12 col-12 pr-0">
                        <div class="question-form">
                            <textarea name="" id="" cols="" rows="" placeholder='Расскажите коротко о проекте, прикрепите ТЗ (если есть) или задайте свой вопрос'></textarea>
                            <img src="image/arrow-input.png" alt="">
                        </div>
                        </div>
                        </div>
                        </div>
                        <div class="col-xl-12">
                            <div class="form-btn-inner">
                                <div class="col-xl-4 pl-0 col-md-6">
                                    <div class="form-btn">
                                    <button>Отправить заявку</button>
                                    </div>
                                </div>
                                <div class="col-xl-8 col-md-6">
                                    <div class="checkbox">
                                     <input type="checkbox" id="checkbox_1">
                                    <label for="checkbox_1">Даю согласие на обработку <a href=''>персональных данных</a></label>
                                    </div>
                                    </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include('footer.php');?>