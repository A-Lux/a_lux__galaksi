<div class="footer">
    <div class="container">
        <div class="footer-inner">
            <div class="row">
                <div class="col-xl-3 col-md-3">
                    <div class="footer-logo">
                        <img src="image/footer-logo.png" alt="">
                    </div>
                </div>
                <div class="col-xl-3 col-md-3">
                    <div class="footer-title">
                        <h1>Адрес</h1>
                    </div>
                    <div class="footer-txt">
                        <p>пр. Рыскулова, 1/7, г. Алматы</p>
                        <p>Республика Казахстан</p>
                        <p>Почтовый индекс 050019</p>
                    </div>
                </div> 
                <div class="col-xl-3 col-md-3">
                <div class="footer-title">
                        <h1>О нас</h1>
                    </div>
                    <div class="footer-txt">
                        <p>О компании Alugal</p>
                        <p>О компании Herkül</p>
                        <p>О компании Galwin</p>
                    </div>
                </div>
                <div class="col-xl-3 col-md-3">
                    <div class="footer-contact">
                        <h2>8 727 399 48 11</h2>
                        <a href="">E-mail: marketing@siegeria.com </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="row">
                <div class="col-xl-6">
                    <div class="foot-bottom d-flex">
                        <p>Все права защищены</p>
                        <p>© Galaksi 2015 All right reserved</p>
                    </div>
                </div>
                <div class="col-xl-2">
                <div class="footer-icons">
                <a href=""><i class="fab fa-facebook-f"></i></a>
                <a href=""><i class="fab fa-instagram"></i></a>
                <a href=""><i class="fab fa-whatsapp"></i></a>
                <a href=""><i class="fab fa-youtube"></i></a>
                </div>
                </div>
                <div class="col-xl-4">
                    <div class="footer-link">
                    <p>Сайт разработан в: <a href="">A-LUX</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="slick/slick.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/main.js"></script>
</body>
</html>