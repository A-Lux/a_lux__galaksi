<?php include('header.php');?>
<section class="vacancy">
    <div class="container">
        <div class="crumb-page">
    <nav aria-label="breadcrumb ">
  <ol class="breadcrumb crumbs">
    <li class="breadcrumb-item crumb-item"><a href="#">Главная</a></li>
    <li class="breadcrumb-item active" aria-current="page">Вакансии
    </li>
  </ol>
</nav>
</div>
    </div>
    <div class="vacancy-content">
    <div class="container">
        <div class="vacancy-title">
            <h3>В GALAKSI GROUP открыты срочные ВАКАНСИИ:</h3>
            </div>
        <div class="vacancy-group">
        <div class="col-xl-6 pl-0">
        <div class="vacancy-inner">
       
            <div class="vacancy-card" class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">  
                <h1>1</h1>
                <p>Продажа</p> 
        </div>
        <div class="collapse" id="collapseExample">
  <div class="card card-body">
    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
  </div>
</div>
            <div class="vacancy-card" class="btn btn-primary" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample2">  
                <h1>2</h1>
                <p>Производство</p> 
</div>
<div class="collapse" id="collapseExample2">
  <div class="card card-body">
    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
  </div>
</div>

            <div class="vacancy-card" class="btn btn-primary" data-toggle="collapse" href="#collapseExample3" role="button" aria-expanded="false" aria-controls="collapseExample3">  
                <h1>3</h1>
                <p>Логистика</p> 
        </div>
        <div class="collapse" id="collapseExample3">
  <div class="card card-body">
    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
  </div>
</div>
    </div>
</div>
    <div class="col-xl-6">
    <div class="vacancy-inner">
        
            <div class="vacancy-card" class="btn btn-primary" data-toggle="collapse" href="#collapseExample4" role="button" aria-expanded="false" aria-controls="collapseExample4">  
                <h1>4</h1>
                <p>Склад</p> 
        </div>
        <div class="collapse" id="collapseExample4">
  <div class="card card-body">
    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
  </div>
</div>
        <div class="vacancy-card"  class="btn btn-primary" data-toggle="collapse" href="#collapseExample5" role="button" aria-expanded="false" aria-controls="collapseExample5">  
                <h1>5</h1>
                <p>Администрация</p> 
        </div>
        <div class="collapse" id="collapseExample5">
  <div class="card card-body">
    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
  </div>
</div>
        <div class="vacancy-card"  class="btn btn-primary" data-toggle="collapse" href="#collapseExample6" role="button" aria-expanded="false" aria-controls="collapseExample6">  
                <h1>6</h1>
                <p>Сервис служба</p> 
        </div>
        <div class="collapse" id="collapseExample6">
  <div class="card card-body">
    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
  </div>
</div>
        </div>
        </div>
</div>
<div class="vacancy-image">
    <div class="col-xl-12 pl-0">
        <img src="image/vacancy.png" alt="">
    </div>
</div>
</div>

   
</section>



<?php include('footer.php');?>