<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="jquery/jquery-ui.css">
    <link rel="stylesheet" href="jquery/jquery-ui.structure.css">
    <link rel="stylesheet" href="jquery/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link rel="stylesheet" href="font/css/all.css">
    <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="./css/main.css">
    <title>Document</title>
</head>

<body>
    <section class="head">
        <div class="container">
            <div class="header-inner">
                <div class="col-xl-3 col-md-3">
                    <div class="logo">
                        <img src="image/logo.png" alt="">
                    </div>
                </div>
                <div class="col-xl-9 col-md-9">
                    <div class="header-links">
                    <nav class="nav-links">
                            <a href="index.php" class="link active">ГЛАВНАЯ</a>
                            <a href="we.php">МЫ</a>
                            <a href="about.php">О КОМПАНИИ</a>
                            <a href="vacancy.php">ВАКАНСИИ</a>
                            <a href="contact.php">КОНТАКТЫ</a>
                    </nav>
                    </div>
                </div>
            </div>
            <div class="burger-menu">
            <div class="burger-menu-image">
            <img src="image/logo.png" alt="">
            </div>
        <div class="burger-menu-content">
        <a href="#" class="burger-menu-btn">
			<span class="burger-menu-lines"></span>
		</a>
      
    </div>
            </div>
    <div class="nav-panel-mobil">
		<div class="container">
			<nav class="navbar-expand-lg navbar-light">
				<ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">
					<li class="nav-item">
						<a class="nav-link" href="#">МЫ</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="contact.html">О КОМПАНИИ</a>
					</li>
					 <li class="nav-item">
            <a class="nav-link" href="news.html">ВАКАНСИИ</a>
         </li>
         <li class="nav-item">
            <a class="nav-link"  href="delivery.html">КОНТАКТЫ</a>
         </li>
    
    </ul>
			</nav>
        </div>
    </div>
          
        <div class="col-xl-4 col-md-5">
                <div class="header-content">
                    <h3>Galaksi Group</h3>
                    <p>Лидер по производству ПВХ и алюминиевых изделий в Казахстане</p>
                    <button>О нас</button>
                </div>
            </div>

        </div>
    </section>
    <div class="container">
        <div class="header-row">
            <div class="row justify-content-center">

                <div class="col-xl-3">
                    <div class="head-item">
                        <img src="image/diamond.png" alt="">
                        <p>27 лет на рынке</p>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="head-item">
                        <img src="image/company.png" alt="">
                        <p>23 крупнейших завода в Средней Азии</p>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="head-item">
                        <img src="image/group.png" alt="">
                        <p>Более 1000 сотрудников в Средней Азии</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="galaksi-group">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                <div class="group-title ">
                    <img src='image/group-logo.png'>
                        <h1>Galaksi Group</h1>
                    </div>
                </div>
                <div class="col-xl-4 p-0">
                    <div class="group-item">
                        <div class="group-image alugal">
                            <img src="image/новый логотип алюгал.png" alt="">
                        </div>
                        <p>ТОО “Alugal”</p>
                    </div>
                </div>
                <div class="col-xl-4 p-0 ">
                    <div class="group-item">
                        <div class="group-image">
                            <img src="image/Novus polymer logo.png" alt="">
                        </div>
                        <div class="group-txt">
                            <p>ТОО “Novus Polymer”</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 p-0">
                    <div class="group-item">
                        <div class="group-image">
                            <img src="image/Fidelis global logo.png" alt="">
                        </div>
                        <p>ТОО “Fidelis Global”</p>
                    </div>
                </div>
                <div class="col-xl-4 p-0">
                    <div class="group-item">
                        <div class="group-image">
                            <img src="image/fifa.png" alt="">
                        </div>
                        <p>ТОО “Fittings Factory”</p>
                    </div>
                </div>
                <div class="col-xl-4 p-0">
                    <div class="group-item siger">
                        <div class="group-image">
                            <img src="image/SIEGER.png" alt="">
                        </div>
                        <p>ТОО “Sieger WDF”</p>
                    </div>
                </div>
                <div class="col-xl-4 p-0">
                    <div class="group-item">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="news">
        <div class="container">
            <div class="news-title">
                <h1>Новости</h1>
            </div>
            <div class="slide-show">
                <div class="slide">
                    <img src="image/news1.png" alt="">
                    <h2>Прохладнее летом!</h2>
                    <button>06.10.2019</button>
                    <p>Специальное покрытие стекла защищает от прямых солнечных</p>
                    <a href="">ПОДРОБНЕЕ...</a>
                </div>
                <div class="slide">
                    <img src="image/news2.png" alt="">
                    <h2>Прохладнее летом!</h2>
                    <button>06.10.2019</button>
                    <p>Специальное покрытие стекла защищает от прямых солнечных</p>
                    <a href="">ПОДРОБНЕЕ...</a>
                </div>
                <div class="slide">
                    <img src="image/news3.png" alt="">
                    <h2>Прохладнее летом!</h2>
                    <button>06.10.2019</button>
                    <p>Специальное покрытие стекла защищает от прямых солнечных</p>
                    <a href="">ПОДРОБНЕЕ...</a>
                </div>
                <div class="slide">
                    <img src="image/news4.png" alt="">
                    <h2>Прохладнее летом!</h2>
                    <button>06.10.2019</button>
                    <p>Специальное покрытие стекла защищает от прямых солнечных</p>
                    <a href="">ПОДРОБНЕЕ...</a>
                </div>
                <div class="slide">
                    <img src="image/news1.png" alt="">
                    <h2>Прохладнее летом!</h2>
                    <button>06.10.2019</button>
                    <p>Специальное покрытие стекла защищает от прямых солнечных</p>
                    <a href="">ПОДРОБНЕЕ...</a>
                </div>
            </div>
        </div>
    </div>
    <div class="galaksi-content">
        <div class="container">
            <div class="content-inner">
                <div class="content-title text-center">
                    <h1>Почему мы</h1>
                </div>
                <div class="row">

                    <div class="col-xl-6 col-lg-6">
                        <div class="slider-inner">
                            <div class="slider-for">
                                <div class="slides">
                                    <img src="image/slide.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="image/news2.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="image/slide.png" alt="">
                                </div>
                            </div>
                            <div class="slider-nav">
                                <div class="slider-links"><img src="image/slide.png" alt=""></div>
                                <div class="slider-links"><img src="image/news2.png" alt=""></div>
                                <div class="slider-links"><img src="image/slide.png" alt=""></div>
                                <div class="slider-links"><img src="image/slide.png" alt=""></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6 col-lg-6">
                        <h2>Lorem ipsum dolor sit amet consectetur.</h2>
                        <div class="text-overflow">
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Neque, aliquid! Sed non, cumque natus harum corporis iusto iure provident qui neque, rerum saepe, iste animi vero eveniet. Necessitatibus, quam! Consequuntur tenetur aut quis autem, atque maxime error at assumenda ab ea cumque corporis repudiandae temporibus. Tenetur omnis excepturi unde illum! Provident, facilis soluta corrupti illum maxime iste dolores nemo quia aliquid, expedita cumque ad asperiores, tenetur et eligendi? Ratione nemo et consectetur fugit aut sint reiciendis recusandae delectus nesciunt nihil, amet molestias neque, doloremque ea deserunt expedita deleniti cumque. Impedit nostrum ipsa maiores vel, voluptatem quaerat temporibus quibusdam consequuntur facilis? 
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Neque,
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Neque,
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Neque,
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Neque,
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Neque,
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Neque,    
                        </p>
                        </div>
                        <div class="read-overflow">
                        <a href="#" class="link-overflow">Читать далее <img src="image/read-arrow.png" alt=""></a>
                    </div>
                    </div>

                </div>

                <div class="arrows d-flex">
                    <div class="col-xl-6 text-right pr-0">
                        <button class="prev-nav"><img src="image/arrow-prev.png" alt=""></button>
                    </div>
                    <div class="col-xl-6">
                        <button class="next-nav"><img src="image/next.png" alt=""></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php include('footer.php');?>